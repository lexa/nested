#!/bin/sh

#TODO ADD CONFIG PATH

#remove unnecessary dirs
rm -rf build dist nested.egg-info

if [ $1 = "--user" ]
  then
    python3 setup.py install --user
    # move config file after local installation with 'pip3 install --user'
    mkdir -p ~/.local/etc
    mkdir -p ~/.local/etc/nested
    cp config.yml ~/.local/etc/nested/.
    cp nested/config/gt.style ~/.local/etc/nested/.
  else
    python3 setup.py install
    # move config file
    mkdir -p /etc/nested
    cp config.yml /etc/nested/.
    cp nested/config/gt.style /etc/nested/.
fi
