#!/usr/bin/env python3
#
# run this script within */nested/. path as follows:
# $ sudo python3 teGreedyNester_Installation.py

import apt
import os
import subprocess
import sys

# check installed tools:
def returnToolDict():
    toolDict = {}
    toolDict = {'build-essential':False, 'gcc':False, 'pip3':False, 'git':False, 'ltr_finder':False, 'blastx':False,
                'gt': False}
    return toolDict

def checkTool(tool, boolVar):    
    result = subprocess.run(['which', '{}'.format(tool)], stdout=subprocess.PIPE)
    path = result.stdout.decode('utf-8').rstrip()
    if tool in path:
        boolVar = True
        return boolVar
    else:
        return boolVar

def runCheck(toolDict):    
    for t in toolDict.keys():
        toolDict[t] = checkTool(t, toolDict[t])
    return toolDict

# 1. installation of apts
# 1.1 'installed or not' check:
def installCheck(toolDict):
    aptList = ['build-essential', 'gcc', 'blast2', 'genometools', 'libgenometools0', 'libgenometools0-dev',
               'python3-pip', 'git']
    if toolDict['build-essential']:
        aptList.remove('build-essential')
        print("'build-essential' is already installed.")
        print()
    if toolDict['gcc']:
        aptList.remove('gcc')
        print("'gcc' is already installed.")
        print()
    if toolDict['blastx']:
        aptList.remove('blast2')
        print("'blastx' is already installed.")
        print()
    if toolDict['gt']:
        aptList.remove('genometools')
        print("'genometools' are already installed.")
        print()
    if toolDict['pip3']:
        aptList.remove('python3-pip')
        print("'python3-pip' is already installed.")
        print()
    if toolDict['git']:
        aptList.remove('git')
        print("'git' is already installed.")
        print()    
    return aptList

# 1.2 Installation:    
def installPckg(apt_name):
    pkg_name = apt_name

    cache = apt.cache.Cache()
    cache.update()
    cache.open()

    pkg = cache[pkg_name]
    if pkg.is_installed:
        print("{pkg_name} is already installed".format(pkg_name = pkg_name))
        print()
    else:
        pkg.mark_install()

        try:
            cache.commit()
        except Exception (arg):
            print("Sorry, package installation failed [{err}]".format(err=str(arg)), file=sys.stderr)
            print()
            
def runInstallation(aptList):
    for apt_name in aptList:
        installPckg(apt_name)
        print()

# 2. ltr_finder
def compileLtrFinder():
    presentPath = os.getcwd()
    os.chdir(presentPath + "/LTR_Finder/source")
    subprocess.check_call(["make"])
    os.chdir(presentPath)
    print()
    
def installLtrFinder(toolDict):
    path2LtrFinder = ""
    if not toolDict['ltr_finder']:
        path2LtrFinder = ""
        if not os.path.exists("LTR_Finder/source/ltr_finder"):
            subprocess.check_call(["git", "clone", "https://github.com/xzhub/LTR_Finder.git"])
            compileLtrFinder()
            path2LtrFinder = os.getcwd() + "/LTR_Finder/source/ltr_finder"
        else:
            compileLtrFinder()
            path2LtrFinder = os.getcwd() + "/LTR_Finder/source/ltr_finder"
        print("'LTR_Finder' has been installed.")
        print()
    else:
        print("'LTR_Finder' is already installed.")
        print()
        result = subprocess.run(['which', 'ltr_finder'], stdout=subprocess.PIPE)
        path2LtrFinder = result.stdout.decode('utf-8').rstrip()
    return path2LtrFinder

# 3. python3 packages:
def installPyPckgs():
    pckgList = ['setuptools', 'biopython', 'bcbio-gff', 'click', 'decorator', 'networkx', 'numpy',
    'PyYAML', 'six']

    for pckg in pckgList:
        subprocess.check_call(['pip3', 'install', '{}'.format(pckg), '--no-cache-dir'])
        print()
        
# 4. teGreedyNester:
# 4.1 setup paths in 'config.yml':
def getPaths(path2LtrFinder):
    path2prosite = os.getcwd() + "/dbs/prosite"
    path2tRNA = os.getcwd() + "/LTR_Finder/source/tRNA/Athal-tRNAs.fa"
    path2style = os.getcwd() + "/nested/config/gt.style"
    path2db = os.getcwd() + "/dbs/gydb_arh_chdcr/gydb_arh_chdcr.fa"
    with open("config.yml", "w") as out:
        with open("config_template.yml") as cfg:
            for l in cfg:
                if "/path/to/ltr_finder_repository/LTR_Finder/source/ltr_finder" in l:
                    lNew = l.replace("/path/to/ltr_finder_repository/LTR_Finder/source/ltr_finder",
                              path2LtrFinder)
                    out.write(lNew)
                elif "path/to/repository/dbs/prosite" in l:
                    lNew = l.replace("path/to/repository/dbs/prosite", path2prosite)
                    out.write(lNew)
                elif "/path/to/ltr_finder_repository/LTR_Finder/source/tRNA/Athal-tRNAs.fa" in l:
                    lNew = l.replace("/path/to/ltr_finder_repository/LTR_Finder/source/tRNA/Athal-tRNAs.fa",
                              path2tRNA)
                    out.write(lNew)
                elif "/path/to/nested/nested/config/gt.style" in l:
                    lNew = l.replace("/path/to/nested/nested/config/gt.style", path2style)
                    out.write(lNew)
                elif "path/to/repository/dbs/gydb_arh_chdcr/gydb_arh_chdcr.fa" in l:
                    lNew = l.replace("path/to/repository/dbs/gydb_arh_chdcr/gydb_arh_chdcr.fa", path2db)
                    out.write(lNew)
                else:
                    out.write(l)
    print("Paths in 'config.yml' have been set.")
    print()
    
# 4.2 install nester
def installNester():
    subprocess.check_call(["./setup.sh"])
    print()
    print("Te-greedy-nester has been installed.")
    print()
    
# Whole process:
if __name__ == "__main__":
    # check installed tools:
    toolDict = returnToolDict()
    toolDictChecked = runCheck(toolDict)

    # 1. apts
    # 1.1 installed or not check:
    # &&
    # 1.2 installation:    
    runInstallation(installCheck(toolDictChecked))

    # 2. ltr_finder installation:
    path2LtrFinder = installLtrFinder(toolDictChecked)

    # 3. python3 packages:
    installPyPckgs()

    # 4. teGreedyNester:
    # 4.1 setup paths in 'config.yml':
    getPaths(path2LtrFinder)
    
    # 4.2 install nester:
    installNester()
