#!/bin/bash

processEcho () {
	echo ""
	echo "#####"
	echo "# $1"
	echo "#####"
	echo ""
}

installApt () {
	# check [$1=binName; $2=aptName]
	if hash $1 2>/dev/null; then
		processEcho "$1 is already installed!"
	else
		if [[ $1 == gt ]]; then
			processEcho "Installing libgenometools0, libgenometools-dev and genometools"
			apt-get install libgenometools0 libgenometools0-dev genometools	
		else
			apt-get install $2
		fi
	fi
}

installPip3Modul () {
	processEcho "Installing $1"
	pip3 install $1
}

# apt dependencies
aptBinNames=(build-essential gcc pip3 git gt blastx)
aptNames=(build-essential gcc python3-pip git genometools blast2)
# add also libgenometools0 libgenometools0-dev  for gt

# pip3 dependencies
pip3Moduls=(setuptools biopython bcbio-gff click decorator networkx numpy PyYAML six)


# main process
if [[ $EUID -ne 0 ]]; then
	echo "You should run as root!"
	exit 1
else
	# Prequisites installation
	processEcho "CHECKING AND INSTALLING DEPENDENCIES"
	processEcho "APT PACKAGES"
	apt-get update
	for i in "${!aptBinNames[@]}"
		do
		installApt "${aptBinNames[$i]}" "${aptNames[$i]}"
	done
	processEcho "PYTHON3 PACKAGES"
	for pckg in "${pip3Moduls[@]}"
		do
		processEcho "Installing $pckg"
		pip3 install $pckg
	done
	
	# generate BLASTDB
	makeblastdb -in dbs/gydb_arh_chdcr/gydb_arh_chdcr.fa -dbtype prot -out dbs/gydb_arh_chdcr/gydb_arh_chdcr.fa
	
	# LTR_Finder installation
	# check
	processEcho "LTR_FINDER CHECK & INSTALLATION"
	path2LtrFinder=""
	if hash ltr_finder 2>/dev/null; then
		processEcho "$1 is already installed!"
		path2LtrFinder="ltr_finder"
	else
		processEcho "LTR_Finder installation"
		git clone https://github.com/xzhub/LTR_Finder.git
		cd LTR_Finder/source
		make
		path=$(pwd)

		path2LtrFinder=$(pwd)"/ltr_finder"
		cd ../../
	fi
	
	# Setup paths in 'config.yml'
	processEcho "GENERATING 'config.yml'"

	# remove old config.yml if exist
	if test -f config.yml; then
	    rm -r config.yml
	fi
	
	# strings to be changed
	ltrFinder="/path/to/ltr_finder_repository/LTR_Finder/source/ltr_finder"
	prosite="path/to/repository/dbs/prosite"
	tRNAs="/path/to/ltr_finder_repository/LTR_Finder/source/tRNA/Athal-tRNAs.fa"
	gtStyle="/path/to/nested/nested/config/gt.style"
	domDb="path/to/repository/dbs/gydb_arh_chdcr/gydb_arh_chdcr.fa"
	origPaths=($ltrFinder $prosite $tRNAs $gtStyle $domDb)
	# real paths	
	path2prosite=$(pwd)"/dbs/prosite"
	path2tRNAs=$(pwd)"/LTR_Finder/source/tRNA/Athal-tRNAs.fa"
	path2gtStyle=$(pwd)"/nested/config/gt.style"
	path2domDb=$(pwd)"/dbs/gydb_arh_chdcr/gydb_arh_chdcr.fa"
	realPaths=($path2LtrFinder $path2prosite $path2tRNAs $path2gtStyle $path2domDb)

	# replacing paths
	IFS=''
	while read -r line
	do
		line=${line//$(echo $ltrFinder | sed 's_/_\\/_g')/$(echo $path2LtrFinder)}
		line=${line//$(echo $prosite | sed 's_/_\\/_g')/$(echo $path2prosite)}
		line=${line//$(echo $tRNAs | sed 's_/_\\/_g')/$(echo 	$path2tRNAs)}
		line=${line//$(echo $gtStyle | sed 's_/_\\/_g')/$(echo $path2gtStyle)}
		line=${line//$(echo $domDb | sed 's_/_\\/_g')/$(echo $path2domDb)}
		echo $line >> config.yml
	done<config_template.yml
	processEcho "'config.yml' was generated"
	
	# installing nester
	processEcho "INSTALLING Te-greedy-nester"
	./setup.sh
	processEcho "Te-greedy-nester has been installed"
fi
