----------------------------------------------------------------------------
        PROSITE protein domains, families and functional sites database
        SIB Swiss Institute of Bioinformatics; Geneva, Switzerland
----------------------------------------------------------------------------

Description: PROSITE sequence logo definition
Name:        sequence_logos.txt
Release:     

----------------------------------------------------------------------------

What is a sequence logo?
========================
A sequence logo is a graphical display of a multiple sequence alignment
consisting of colour-coded stacks of letters representing amino acids at
successive positions. Sequence logos provide a richer and more precise
description of sequence similarity than consensus sequences and can
rapidly reveal significant features of the alignment that could otherwise
be difficult to perceive.

The total height of a logo position depends on the degree of conservation
in the corresponding multiple sequence alignment column. Very conserved
alignment columns produce high logo positions.

The height of each letter in a logo position is proportional to the observed
frequency of the corresponding amino acid in the alignment column.

The letter of each stack is ordered from most to least frequent, so that it
is possible to read the consensus sequence from the top of the stacks.


PROSITE sequence logos
======================
The sequence logos available from the PROSITE WebSite have been build using
WebLogo (http://weblogo.berkeley.edu/).

'#' in a sequence logo figure means the number of true positive hits
detected in UniProtKB/Swiss-Prot used to build the sequence logo.
Sequence logos aren't generated if the number of true positive hits in
UniProtKB/Swiss-Prot is below four.

For patterns, each position is shown in the logo, whereas for profiles
only match positions are considered, i.e. the length of the logo corresponds
to the length of the profile.


  ------------------------------------------------------------------------
  PROSITE is copyright.   It  is  produced  by  the SIB Swiss  Institute
  of Bioinformatics. There are no restrictions on its use by non-profit
  institutions as long as its  content is in no way modified. Usage by and
  for commercial  entities requires a license agreement.   For information
  about  the  licensing  scheme   send  an  email to license@sib.swiss or
  see: http://prosite.expasy.org/prosite_license.html.
  ------------------------------------------------------------------------
