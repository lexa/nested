# nested

## Introduction

*nested* (now also called *TE-greedy*) is software to analyze nested LTR transposable elements 
in DNA sequences, such as reference genomes. It is made of two components: *nested-generator* 
for generating simulated sequences of nested retrotransposons, and *nested-nester* (now called 
*TE-greedy-nester*) that looks for nested, as well as non-nested and solo-LTR repeat sequences 
in the input. Unlike other similar software, *TE-greedy-nester* is structure-based by using 
de-novo retrotransposon identification software *LTR Finder*, relying on sequence information 
only secondarily.
A virtual machine with TE-greedy-nester installed is available at http://hedron.fi.muni.cz/TE-nester_Mint.zip

## External tools
Program is using other external tools. Before usage make sure you have the following tools installed:

- LTR finder (v 1.05+) -
http://www.mybiosoftware.com/ltr_finder-1-0-5-find-full-length-ltr-retrotranspsons-genome-sequences.html
- blastx (v 2.2.31+) -
https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download
- genometools (v 1.5.8) with gt sketch -
https://github.com/genometools/genometools

## Virtual machine
An Ubuntu virtual machine with the latest version of TE-greedy-nester installed is available at http://hedron.fi.muni.cz/TEgnester

## Docker container

Folder "Docker" contains necessary files to build Docker Image.  
Example how to build or rebuild:

```bash
# building image
# get in folder with "Dockerfile"
cd Docker
docker build --progress=plain -t nester .

# to rebuild use "--no-cache"
docker build --no-cache --progress=plain -t nester .
```

Prebuild [Docker image](https://hub.docker.com/r/tegreedy/nested) is available from docker hub.  
Example how to run:

```bash
# help
docker run tegreedy/nested:2.0.0 nested-nester --help
```

```bash
# create woking and output directories
mkdir -p data/nester_out && cd data
# get test data
wget https://gitlab.fi.muni.cz/lexa/nested/-/raw/master/test_data/151kb_adh1_bothPrimes.fasta
# run nester
docker run -v ${PWD}:/data tegreedy/nested:2.0.0 nested-nester -d /data/nester_out -n 4 /data/151kb_adh1_bothPrimes.fasta
```

## Installation - a step-by-step guide
The following is an installation procedure that works on our recent Linux Ubuntu based machines.

### Prerequisites

#### ncbi blast+

```
sudo apt install blast2
```
For user-space installation from source, please, follow https://www.ncbi.nlm.nih.gov/books/NBK279671/ with version 2.2.31+

#### LTR_finder

```
git clone https://github.com/xzhub/LTR_Finder.git
cd LTR_Finder/source
make
```

#### genome tools

```
sudo apt-get install genometools
sudo apt-get install libgenometools0 libgenometools0-dev
```
For user-space installation use:

```
git clone https://github.com/genometools/genometools.git
cd genometools
make -j4
make -j4 install prefix=~/gt
```
See GenomeTools repository for modifications and trouble-shooting of the installation procedure

#### python requirements using pip3

```
sudo pip3 install bcbio-gff==0.6.4 click==6.7 decorator==4.2.1 networkx==2.1 numpy==1.14.2
PyYAML==3.12 six==1.11. --no-cache-dir
```
For user-space installation use:

```
pip3 install bcbio-gff==0.6.4 click==6.7 decorator==4.2.1 networkx==2.1 numpy==1.14.2
PyYAML==3.12 six==1.11. --no-cache-dir --user
```
Note1: the version numbers are minimal recommended versions, on many systems you can leave those out.

Note2: With user-space installation of Python software you may need to set PYTONPATH accordingly.

First check its value, not to overwrite something needed by your system:

```
echo $PYTHONPATH
```
Setting can be done with
```
export PYTHONPATH="~/.local/bin"
```
Separate multiple directory entries with a colon.

### TE-greedy-nester

#### clone whole directory from gitlab:

```
git clone https://gitlab.fi.muni.cz/lexa/nested.git
cd nested
```

#### modify respective paths in 'config_template.yml':

```
cp config_template.yml config.yml
vi config.yml
```
Note: we're looking into ways to automate this step

#### after 'config.yml' is created call:

```
sudo bash ./setup.sh
```

#### For user-space installation of TE-greedy-nester call:

```
bash ./setup.sh --user
```


#### that’s all!

## Setting up the config
Before usage, **config.yml** needs to be set up:

- ltr finder paths - executable, prosite and tRNAdb
- ltr finder arguments 
- genome tools sketch path
- genome tools sketch arguments
- blastx protein database (installed together with TE-greedy-nester)
- blastx arguments

**Note**: `config.yml` is located by default in directory `/etc/nested/.` after 
normal installation or `~/.local/etc/nested/.` after user space installation 
using the --user switch with setup.sh or setup.py 

## Usage
```
Usage: nested-nester [OPTIONS] INPUT_FASTA

Options:
  -s, --sketch_only       If true, nesting is not computed. Genes are sketched
                          only from existing gff files.
  -d, --data_folder TEXT  Output data folder.
  -dt --discovery-tool TEXT       Used discovery tool. Default is LTR-finder.
  --help                  Show this message and exit.
```

```
Usage: nested-generator [OPTIONS] INPUT_DB OUTPUT_DB

Options:
  -l, --baselength INTEGER        Baselength for generated elements.
  -i, --number_of_iterations INTEGER
                                  Number of iterations in generating.
  -n, --number_of_elements INTEGER
                                  Number of generated elements.
  -f, --filter                    Filter database and create new one with
                                  given output db path.
  -s, --filter_string TEXT        Filter entries by given string [ONLY
                                  RELEVANT WITH -filter OPTION].
  -o, --filter_offset INTEGER     LTR offset allowed [ONLY RELEVANT WITH
                                  -filter OPTION].
  -d, --data_folder TEXT          Output data folder.
  --help                          Show this message and exit.
```

## Adding new LTR discovery tool

* Implement run() method in new TE class based on core/TE_template.py

* Add new value to core/DiscoveryTool.py including any aliases, make sure the numeric value is different from already implemented tools and that it is the same for all of your aliases. (used in calling nester ``-dt <value you set>``)

* Import your newly implemented class in utils/DependencyResolver.py and add it to ``valueToClassDict`` attribute. (key is numeric value you set in core/DiscoveryTool.py)
