#!/usr/bin/perl -w

#
# A script to add TSDs (from another FASTA file that is a sole argument of the command, 
# wildcards allowed) to sequences in a FASTA file (input, output).
#
# Author: Matej Lexa
# Date: 2019/11/19
#

$sequence = "";
$files = $ARGV[0];

while($_=<STDIN>){

  if($_=~/^>/){
    if($sequence ne ""){
      $tsd = `grep -v \"^>\" $files \| grep . \| shuf \| head -1 \| egrep -o \"[A-Za-z]+\$\" \| cat`;
      chop($tsd);
      print "$tsd$sequence$tsd\n\n";
    }
    $sequence = "";
    print $_;
  } else {
    chop($_);
    $sequence .= uc($_);
  }

}

$tsd = `grep -v \"^>\" $files \| grep . \| shuf \| head -1 \| egrep -o \"[A-Za-z]+\$\" \| cat`;
chop($tsd);
print "$tsd$sequence$tsd\n\n";

exit(1);

