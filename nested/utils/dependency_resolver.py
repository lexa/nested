#!/usr/bin/env python3

from nested.core.te_harvest import TE_harvest #ltr harvest
from nested.core.te import TE #ltr finder
from nested.core.discovery_tool import DiscoveryTool

class DependencyResolver():
    def __init__(self, tool_name):
        self.valueToClassDict = {1: TE, 2: TE_harvest}
        self.discovery_tool = self.resolve_discovery_tool(tool_name)

    def resolve_discovery_tool(self, tool_name):        
        value = 1
        if (tool_name in DiscoveryTool.__members__):
            value = DiscoveryTool[tool_name].value
        return self.valueToClassDict[value]
