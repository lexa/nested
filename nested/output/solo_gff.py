#!/usr/bin/env python3
import shutil
import os

class SoloGFFMaker(object):

    def __init__(self):
        pass

    def create_solo_gff(self, solo_ltrs, dirpath):
        with open("{}/{}/{}_solo_ltrs.gff".format(dirpath, solo_ltrs.seqId, solo_ltrs.seqId), 'w+') as gff:
            i = 0
            gff.write("##gff-version 3\n")
            for solo in solo_ltrs.solo_ltrs:
                gff.write(solo_ltrs.seqId + '\t'
                          + "feature\tsolo_ltr\t"
                          + str(solo.location[0]) + '\t'
                          + str(solo.location[1]) + '\t'
                          + ".\t.\t.\t"
                          + "ID=solo_ltr_" + str(i)
                          + '\n')
                i += 1
                  
    def move_ltrs_spliced(self, seqId, dirpath):
        if os.path.exists("{}_ltrs.fa".format(seqId)):
            shutil.move("{}_ltrs.fa".format(seqId), "{}/{}/{}_ltrs.fa".format(dirpath, seqId, seqId))
        if os.path.exists("{}_spliced.fa".format(seqId)):
            shutil.move("{}_spliced.fa".format(seqId), "{}/{}/{}_spliced.fa".format(dirpath, seqId, seqId))        
                  
                          
        
