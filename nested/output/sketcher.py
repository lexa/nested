#!/usr/bin/env python3

import os
import subprocess

from nested.config.config import config, args_dict_to_list
from nested.output.gff import GFFMaker
from nested.output.solo_gff import SoloGFFMaker
from nested.output.trf_gff import TrfGFFMaker

DEFAULT_DIRPATH = 'data'

class Sketcher(object):
    def __init__(self):
        self._gff_maker = GFFMaker()
        self._solo_gff_maker = SoloGFFMaker()
        self._trf_gff_maker = TrfGFFMaker()
        self._gff_path = ''

    def create_gff(self, nested_element, dirpath, output_fasta_offset=0, format='default'):
        path = os.path.join(dirpath, DEFAULT_DIRPATH)
        self._gff_maker.create_gff(nested_element, path, output_fasta_offset, format)

    def create_solo_ltr_gff(self, solo_ltrs, dirpath):
        path = os.path.join(dirpath, DEFAULT_DIRPATH)
        self._solo_gff_maker.create_solo_gff(solo_ltrs, path)
        self._solo_gff_maker.move_ltrs_spliced(solo_ltrs.seqId, path)
    
    def create_trf_gff(self, trf, seqId, dirpath):
        path = os.path.join(dirpath, DEFAULT_DIRPATH)
        self._trf_gff_maker.create_gff(trf, seqId, path)

    def sketch(self, id, dirpath):
        path = os.path.join(dirpath, DEFAULT_DIRPATH)
        null = open(os.devnull, 'w')
        try:
            process = subprocess.check_output(
                [config['gt']['path'], config['gt']['command']] + 
                args_dict_to_list(config['gt']['args']) + 
                ['{}/{}/{}.png'.format(path, id, id)] + 
                ['{}/{}/{}.gff'.format(path, id, id)], stderr=null)
        except Exception:
            command = " ".join([config['gt']['path'], config['gt']['command']] + 
            args_dict_to_list(config['gt']['args']) + 
            ['{}/{}/{}.png'.format(path, id, id)] + 
            ['{}/{}/{}.gff'.format(path, id, id)])
            raise Exception("Command \'{}\' returned non-zero status code. Problem with package genometools.".format(command))
