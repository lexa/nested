import re
import os

from nested.config.config import config

def addColors(fileName):
    output = open("{}_cols.gff".format(os.path.splitext(fileName)[0]), 'w')
    with open(fileName, 'r') as file:
        for line in file:
            output.write(line.strip())
            name = re.search(r"(?<=name=)[^;]*", line)
            if name:
                feature = name.group(0).strip().replace(" ", "_")
                if feature in config['igv_colors'].keys():                
                    output.write(";color=" + config['igv_colors'][feature])
                else:
                    output.write(";color=" + config['igv_colors']['default'])
            output.write('\n')
