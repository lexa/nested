#!/usr/bin/env python3

class TrfGFFMaker(object):

    def __init__(self):
        pass

    def create_gff(self, trf, seqId, dirpath):
        with open("{}/{}/{}_trf.gff".format(dirpath, seqId, seqId), 'w+') as gff:
            i = 0
            gff.write("##gff-version 3\n")
            for repeat in trf:
                gff.write(seqId + '\t'
                          + "feature\ttandem_repeat\t"
                          + str(repeat.location[0]) + '\t'
                          + str(repeat.location[1]) + '\t'
                          + ".\t.\t.\t"
                          + "ID=tandem_repeat_" + str(i)
                          + ";monomer_count=" + str(repeat.copies)
                          + ";monomer=" + repeat.monomer
                          + '\n')
                i += 1