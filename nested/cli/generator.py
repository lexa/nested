#!/usr/bin/env python3

import os
import click
from datetime import datetime

from nested.core.generator import Generator
from nested.output.sketcher import Sketcher

@click.command()
@click.argument('input_db', required=True, type=click.Path(exists=True))
@click.argument('output_db', required=True, type=str)
@click.option('--baselength', '-l', type=int, help='Baselength for generated elements.')
@click.option('--number_of_iterations', '-i', type=int, help='Number of inserted elements.')
@click.option('--number_of_elements', '-n', type=int, help='Number of generated sequences.')
@click.option('--filter', '-f', is_flag=True, default=False, type=str, help='Filter database and create new one with given output db path.')
@click.option('--filter_string', '-s', type=str, help='Filter entries by given string [ONLY RELEVANT WITH -filter OPTION].')
@click.option('--filter_offset', '-o', type=int, help='LTR offset allowed [ONLY RELEVANT WITH -filter OPTION].')
@click.option('--percentage', '-p', type=int, help='Percentage of elements in generated sequence.')
@click.option('--average_element', '-a', type=int, help='Average element length in database.')
@click.option('--expected_length', '-e', type=int, help='Expected output sequence length [ONLY WORKS WITH -percentage and -average_element].')
@click.option('--output_directory', '-d', default=os.getcwd(), type=str, help='Output directory.')
def main(input_db, output_db, baselength, number_of_iterations,
         number_of_elements, filter, filter_string, filter_offset,
         percentage, average_element, expected_length, output_directory):
    #number_of_errors = 0
    start_time = datetime.now()
    generator = Generator(input_db)
    if filter:
        params = {}
        if filter_string: params['filter_string'] = filter_string
        if filter_offset: params['ltr_offset'] = filter_offset
        params['verbose'] = True
        generator.filter_db(output_db, **params)
    else:
        params = {}
        if baselength: params['baselength'] = baselength
        if number_of_iterations: params['number_of_iterations'] = number_of_iterations
        if number_of_elements: params['number_of_elements'] = number_of_elements
        if percentage and average_element:
            if expected_length:
                params['baselength'] = calc_baselength(expected_length, percentage)
            if 'baselength' in params.keys():
                params['number_of_iterations'] = calc_number_of_iterations(params['baselength'], percentage, average_element)

        generator.generate_random_nested_elements(**params)

        if not os.path.exists('{}/generated_data'.format(output_directory)):
            os.makedirs('{}/generated_data'.format(output_directory))
        generator.save_elements_to_fasta('{}/generated_data/{}'.format(output_directory, output_db))
        sketcher = Sketcher()
        for element in generator.elements:
            sketcher.create_gff(element, '{}/generated_data'.format(output_directory))
            # sketcher.sketch(element.id, 'generated_data')
    end_time = datetime.now()
    print('Total time: {}'.format(end_time - start_time))
    #print('Number of errors: {}'.format(number_of_errors))

def calc_number_of_iterations(length, percentage, average_element):
     p = percentage / 100
     remainder = int(p / (1 - p) * length)
     return remainder // average_element

def calc_baselength(expected_length, percentage):
    p = percentage / 100
    return int((1 - p) * expected_length)
    

if __name__ == '__main__':
    main()
