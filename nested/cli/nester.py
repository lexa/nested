#!/usr/bin/env python3

import os
import click
import glob
from concurrent import futures
from datetime import datetime
from subprocess import CalledProcessError

from Bio import SeqIO

from nested.core.nester import Nester
from nested.output.sketcher import Sketcher
from nested.core.discovery_tool import DiscoveryTool
from nested.utils.dependency_resolver import DependencyResolver


@click.command()
@click.argument('input_fasta', required=True, type=click.Path(exists=True))
@click.option('--sketch', '-s', is_flag=True, default=False, help='Sketch output.')
@click.option('--format', '-f', default='genome_browser', help='Format for GFF.')
@click.option('--output_fasta_offset', '-o', default=0, help='Number of bases around the element included in output fasta files.')
@click.option('--output_folder', '-d', default="", type=click.Path(writable=True), help='Output data folder.')
@click.option('--initial_threshold', '-t', type=int, default=500, help='Initial threshold value.')
@click.option('--threshold_multiplier', '-m', type=float, default=1, help='Threshold multiplier.')
@click.option('--threads', '-n', type=int, default=1, help='Number of threads')
@click.option('--discovery_tool', '-dt', default=DiscoveryTool.LTR_finder.name,
    type=click.Choice(list(map(lambda val: val, DiscoveryTool.__members__))),
    help='Determines which tool is used for retrotransoson discovery. Default: LTR_finder')
@click.option('--solo_ltrs', '-solo', is_flag=True, default=False, help='Run solo LTR module')
def main(input_fasta, sketch, format, output_fasta_offset, output_folder, initial_threshold, 
        threshold_multiplier, threads, discovery_tool, solo_ltrs):
    check_ram(input_fasta, threads)
    check_permissions(output_folder, os.W_OK | os.X_OK)
    number_of_errors = [0]
    start_time = datetime.now()
    sketcher = Sketcher()
    futuress = []
    dependencyResolver = DependencyResolver(discovery_tool)
    with futures.ThreadPoolExecutor(threads) as executor:
        for sequence in SeqIO.parse(input_fasta, 'fasta'):
            futuress.append(executor.submit(process_sequence, sequence, sketcher, 
                sketch, format, output_fasta_offset, output_folder, 
                initial_threshold, threshold_multiplier, number_of_errors,
                dependencyResolver, solo_ltrs))
        futures.wait(futuress)
    end_time = datetime.now()
    print('Total time: {}'.format(end_time - start_time))
    print('Number of errors: {}'.format(number_of_errors[0]))

def process_sequence(sequence, sketcher, sketch, format, output_fasta_offset, output_folder, initial_threshold, threshold_multiplier, errors, dependency_resolver, solo_ltrs):
    sequence.id = sequence.id.replace('/', '--')
    seq_start_time = datetime.now()
    strlen = 15
    print("Processing {}".format(sequence.id))
    try:
        nester = Nester(sequence, initial_threshold, threshold_multiplier, dependency_resolver, solo_ltrs)
        sketcher.create_gff(nester.nested_element, dirpath=output_folder, output_fasta_offset=output_fasta_offset, format=format)
        
        if solo_ltrs:
            sketcher.create_solo_ltr_gff(nester.solo_ltrs, dirpath=output_folder)
            
        sketcher.create_trf_gff(nester.trf, nester.seqid, dirpath=output_folder)
        if sketch:
            if format != 'default':
                sketcher.create_gff(nester.nested_element, dirpath=output_folder, output_fasta_offset=output_fasta_offset)
            sketcher.sketch(sequence.id, dirpath=output_folder)
        seq_end_time = datetime.now()
        print('Processing {a}: DONE [{b}]'.format(a=sequence.id, b=seq_end_time - seq_start_time))
    except KeyboardInterrupt:
        cleanup()
        raise
    except CalledProcessError as ex:
        cleanup()
        errors[0] += 1
        print('Processing {}: SUBPROCESS ERROR: {}'.format(sequence.id[:strlen], ex))
    except Exception as ex:
        cleanup()
        errors[0] += 1
        print('Processing {}: UNEXPECTED ERROR: {}'.format(sequence.id[:strlen], ex))

def check_permissions(path, permissions):
    path = os.getcwd() if len(path) == 0 else path
    if not os.path.exists('{}/data'.format(path)):
        try:
            os.makedirs('{}/data'.format(path))
        except PermissionError:
            raise click.BadParameter("Path \"{}\" is not writable.".format(path), param=path, param_hint='\"--output_folder\" / \"-d\"')
        except Exception:
            return        

def check_ram(input_file, n):
    mem_bytes = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')
    mem_gib = mem_bytes/(1024**3)
    seq_sizes = []
    for seq in SeqIO.parse(input_file, 'fasta'):
        seq_sizes.append(len(seq) / (1024**3))
    seq_sizes.sort()
    if (sum(seq_sizes[-n:]) * 15) > (0.8 * mem_gib):
        print("\033[93m" + "During the analysis memory usage may exceed 80% of system's total physical memory." + "\033[0m")

def cleanup():
    for file in glob.glob("*ltrs.fa"):
        os.remove(file)
        
if __name__ == '__main__':
    main()
