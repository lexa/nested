import yaml
import os.path
from os import path
from os.path import expanduser

# depending on whether nester was installed with the --user argument locally or not
home = os.path.expanduser("~")
if path.exists(home + '/.local/etc/nested/config.yml'):
    config_path = home + '/.local/etc/nested/config.yml'
else:
    config_path = '/etc/nested/config.yml'
    
with open(config_path) as stream:
    config = yaml.load(stream)


def args_dict_to_list(args):
    args_list = []
    for arg in args:
        args_list += ['-{}'.format(arg), str(args[arg])]
    return args_list
