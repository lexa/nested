#!/usr/bin/env python3

from nested.core.nester import Nester
from nested.core.tandem_repeat import TandemRepeat
from nested.core.tools.tandem_repeat_finder import TandemRepeatFinder

class Executor():
    def __init__(self, sequence):
        self.sequence = sequence
        self.tool_order = [TandemRepeatFinder(), Nester()]

    def run_main_loop(self):
        elements = []
        for tool in self.tool_order:
            elements += tool.run(self.sequence)
    
    def _crop_sequence(self, elements):
        for element in elements:
            self.sequence = self.sequence[:element.location[0]] + self.sequence[element.location[1]:]