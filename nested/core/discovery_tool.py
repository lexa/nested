#!/usr/bin/env python3

from enum import Enum

class DiscoveryTool(Enum):
    LTR_finder = 1
    LTRharvest = 2
    #add aliases with same value as the tool above
    finder = 1
    harvest = 2

