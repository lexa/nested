#!/usr/bin/env python3

import os
import subprocess
import re

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

from nested.config.config import config, args_dict_to_list


class TE(object):
    """Class representing TE. Every location is in format [from, to].

    Attributes:
        ppt (list): location of ppt
        pbs (list): location of pbs
        location (list): position of TE in gene
        ltr_left_location (list): location of left ltr
        ltr_right_location (list): location of right ltr
        tsr_left (list): location of left tsr
        tsr_right (list): location of right tsr
        features (dict): dictionary of features assigned to TE (i.e. list of domains on the optimal path in graph)
        score (float): evaluated score of TE
    """

    def __init__(self, ppt=[0, 0], pbs=[0, 0], location=[0, 0],
                 ltr_left_location=[0, 0], ltr_right_location=[0, 0],
                 tsr_left=[0, 0], tsr_right=[0, 0], features={}, score=None):
        self.ppt = ppt
        self.pbs = pbs
        self.location = location
        self.ltr_left_location = ltr_left_location
        self.ltr_right_location = ltr_right_location
        self.tsr_left = tsr_left
        self.tsr_right = tsr_right
        self.features = features
        self.score = score

    def __str__(self):
        lines = ['{{location = {},'.format(self.location),
                 ' left ltr = {},'.format(self.ltr_left_location),
                 ' right ltr = {},'.format(self.ltr_right_location),
                 ' left tsr = {},'.format(self.tsr_left),
                 ' right tsr = {},'.format(self.tsr_right),
                 ' ppt = {},'.format(self.ppt),
                 ' pbs = {},'.format(self.pbs),
                 # ' features = {},'.format(self.features),
                 ' score = {}}}'.format(self.score)]
        return '\n'.join(lines)
    
    @staticmethod
    def run(seqid, sequence):
        """Run discovery tool of choice on sequence and return list of transposons
    
        Arguments:
            seqid (str): sequence id
            sequence (Bio.Seq.Seq): sequence
    
        Returns:
            list[TE]: list of found ltr pairs as a TE class
        """
        
