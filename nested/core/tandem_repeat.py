#!/usr/bin/env python3
import os
import subprocess
from itertools import islice

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

from nested.core.te import TE
from nested.utils import intervals

class TandemRepeat(TE):
    def __init__(self, loc, period_size, copies, matches, indels, scor, entropy, monomer):
        super().__init__(location=loc, score=scor, ltr_left_location=[float('nan'), float('nan')])
        self.matches = matches
        self.indels = indels
        self.entropy = entropy
        self.period_size = period_size
        self.copies = copies
        self.monomer = monomer

    def is_better(self, other):
        if not isinstance(other, TandemRepeat):
            return False

        score = 0
        score += self.matches > other.matches
        score += self.indels < other.indels
        score += self.score > other.score
        score += self.entropy > other.entropy
        if score == 2:
            return intervals.length(self.location) > intervals.length(other.location)
        return score > 2

    def __str__(self):
        lines = ['location: {}'.format(self.location),
                 'period size: {}'.format(self.period_size),
                 'number of copies: {}'.format(self.copies),
                 '% of matches: {}'.format(self.matches),
                 '% of indels: {}'.format(self.indels),
                 'score: {}'.format(self.score),
                 'entropy: {}'.format(self.entropy)]
        return os.linesep.join(lines)

def run(seqid, sequence):
    if not os.path.exists('/tmp/nested'):
            os.makedirs('/tmp/nested')
    
    if not os.path.exists('/tmp/nested/trf'):
        os.makedirs('/tmp/nested/trf')
    
    with open('/tmp/nested/trf/{}.fa'.format(seqid), 'w+') as tmp_file:
        SeqIO.write(SeqRecord(sequence, id=seqid),
            tmp_file,
            'fasta')
    
    process = subprocess.Popen(
            ['trf', '/tmp/nested/trf/{}.fa'.format(seqid), str(2), str(5), str(7),
            str(80), str(10), str(50), str(2000), '-m', '-d', '-h'],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL)
    
    stdout, stderr = process.communicate()
    
    repeats = filter_candidates(get_candidates(seqid))
    repeats.sort(key=lambda r: r.location[0], reverse=True)
    return repeats

def get_candidates(seqid):
    candidates = []

    with open('{}.fa.2.5.7.80.10.50.2000.dat'.format(seqid)) as file:
        for entry in islice(file, 15, None):
            split = entry.split(' ')
            candidates.append(TandemRepeat([int(split[0]), int(split[1])], int(split[2]), float(split[3]),
                    int(split[5]), int(split[6]), int(split[7]), float(split[12]), split[-1]))

    cleanup(seqid)
    
    return candidates

def filter_candidates(candidates):
    candidates.sort(key=lambda r: r.location[0])
    repeats = []

    for candidate in candidates:
        add = True
        for candidate2 in candidates:
            if candidate2.location[0] > candidate.location[1]:
                break
            if intervals.contains(candidate.location, candidate2.location):
                continue
            elif intervals.intersect(candidate.location, candidate2.location):
                if not candidate.is_better(candidate2):
                    add = False
                    break
        for repeat in repeats:
            if repeat.location[0] > candidate.location[1]:
                break
            if intervals.contains(repeat.location, candidate.location):
                add = False
                break
        if add:
            repeats.append(candidate)
    return repeats

def cleanup(seqid):
    if os.path.isfile("{}.fa.2.5.7.80.10.50.2000.dat".format(seqid)):
        os.remove("{}.fa.2.5.7.80.10.50.2000.dat".format(seqid))
    if os.path.isfile("{}.fa.2.5.7.80.10.50.2000.mask".format(seqid)):
        os.remove("{}.fa.2.5.7.80.10.50.2000.mask".format(seqid))



