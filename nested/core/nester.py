#!/usr/bin/env python3

import math

from nested.utils import intervals
from nested.core.gene import Gene
from nested.core.nested_element import NestedElement
from nested.core.solo_ltrs import SoloLtrs
from nested.logging.logger import NesterLogger
from nested.config.config import config
from nested.core.tool_interface import ToolInterface
import nested.core.tandem_repeat as tandemRepeatFinder


class Nester(ToolInterface):
    """Class represent nesting in sequence, recursivelly find best evaluated transposon and crop it until no new transposons found

    Attributes:
        seqid (str): id of sequence
        sequence (Bio.Seq.Seq): sequence
        nested_element (NestedElement): nested element
    """

    def __init__(self, sequence, threshold, multiplier, dependency_resolver, solo):
        self.seqid = sequence.id
        self.sequence = sequence.seq
        self.nested_element = None
        self.solo_ltrs = SoloLtrs(sequence.id)
        self._iteration = 0
        self._logger = NesterLogger('{}/{}'.format(config['logdir'], self.seqid))
        self.threshold = threshold
        self.multiplier = multiplier
        self.dependency_resolver = dependency_resolver
        self.trf = []
        self.solo = solo
        self._find_nesting()

    def _find_nesting(self):
        # tandem repeat finder
        self.trf = tandemRepeatFinder.run(self.seqid, self.sequence)
        nested_list = self.trf[:]
        
        cropped_sequence = self._crop_sequence(nested_list, self.sequence)
        
        nested_list += self._get_unexpanded_transposon_list(cropped_sequence, self.threshold)  # find list of nested transposons
        nested_list = self._expand_transposon_list(nested_list)

        nested_list = self._filter_nested_list(nested_list)
     
        self.nested_element = NestedElement(self.seqid, self.sequence, nested_list)
        
    def _get_unexpanded_transposon_list(self, sequence, threshold):
        # recursivelly find and crop best evaluated transposon, return unexpanded list of found transposons
        self._iteration += 1
        gene = Gene(self.dependency_resolver, self.seqid, sequence)
        candidates = gene.get_candidates_above_threshold(threshold)
        if not candidates:
            best_candidate = gene.get_best_candidate()
            if not best_candidate:
                if self.solo:
                    self.solo_ltrs.find_solo_ltrs(sequence)
                    self.solo_ltrs.solo_ltrs.sort(key=lambda x: x.location[0], reverse=True)
                    return self.solo_ltrs.solo_ltrs
                return []
            candidates = [best_candidate]

        # sort by score (from highest)
        candidates.sort(key=lambda x: x.score, reverse=True)
        # remove intersections
        nested_list = []
        for candidate in candidates:
            choose = True
            for element in nested_list:
                if intervals.intersect(candidate.location, element.location):
                    choose = False
                    break
            for other_candidate in candidates:
                if candidate.location == other_candidate.location:
                    continue
                if intervals.contains(candidate.location, other_candidate.location):
                    choose = False
                    break
            if choose:
                nested_list.append(candidate)
        # sort by location (reverse)
        nested_list.sort(key=lambda x: x.location[0], reverse=True)
        # crop from sequence
        cropped_sequence = sequence

        self.solo_ltrs.create_ltr_multifasta(nested_list, sequence)
        
        for element in nested_list:
            cropped_sequence = cropped_sequence[:(element.location[0])] + cropped_sequence[
                                                                              (element.location[1]):]   #originally:"(element.location[0] - 1)" and "(element.location[1] + 1):]"

        # LOG
        self._logger.log_iteration(self._iteration, nested_list)

        nested_list += self._get_unexpanded_transposon_list(cropped_sequence, self.multiplier * threshold)

        return nested_list

    def _expand_transposon_list(self, nested_list):
        # backwards expanding of intervals according to previously found and cropped elements
        for i in reversed(range(len(nested_list) - 1)):
            for j in range(i + 1, len(nested_list)):
                nested_list[j].location = intervals.expand(nested_list[i].location, nested_list[j].location)
                nested_list[j].ltr_left_location = intervals.expand(nested_list[i].location,
                                                                    nested_list[j].ltr_left_location)
                nested_list[j].ltr_right_location = intervals.expand(nested_list[i].location,
                                                                     nested_list[j].ltr_right_location)
                for domain in nested_list[j].features['domains']:
                    domain.location = intervals.expand(nested_list[i].location, domain.location)
                if 'ppt' in nested_list[j].features.keys():
                    nested_list[j].features['ppt'] = intervals.expand(nested_list[i].location,
                                                                  nested_list[j].features['ppt'])
                if 'pbs' in nested_list[j].features.keys():                                                               
                    nested_list[j].features['pbs'] = intervals.expand(nested_list[i].location,
                                                                  nested_list[j].features['pbs'])
                nested_list[j].tsr_left = intervals.expand(nested_list[i].location,
                                                           nested_list[j].tsr_left)
                nested_list[j].tsr_right = intervals.expand(nested_list[i].location,
                                                            nested_list[j].tsr_right)
        return nested_list

    def _filter_nested_list(self, nested_list):
        # return list(filter(lambda x: not math.isnan(x.ltr_left_location[0]), nested_list)
        result = []
        for te in nested_list:
            if not math.isnan(te.ltr_left_location[0]):
                result.append(te)
        return result
    
    def _crop_sequence(self, elements, sequence):
        cropped = sequence
        for element in elements:
            cropped = cropped[:element.location[0]] + cropped[element.location[1]:]
        return cropped
    
    
