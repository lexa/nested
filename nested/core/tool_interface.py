#!/usr/bin/env python3

class ToolInterface():
    def run(self, sequence):
        """Runs selected tool, if any arguments are needed they are taken from config.yml or constructor
        Returns: collection of BaseElement
        """        
        pass