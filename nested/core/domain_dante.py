#!/usr/bin/env python3

import subprocess
import os

from io import StringIO

from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastxCommandline

from nested.config.config import config


class DomainDante(object):
    """Class representing domain parsed from blastx output.

    Atrributes:
        evalue (float): evalue (same as blastx output)
        frame (tuple): frame (same as blastx output)
        score (float): score (same as blastx output)
        title (str): title (same as blastx output)
        location (list): location [from, to] (from > to for negative strand)
        type (str): type of domain (AP, GAG, INT, ...)
    """

    def __init__(self, evalue=None, frame=None, score=None, title=None, location=None, domainType=None, annotation=None):
        self.evalue = evalue
        self.frame = frame
        self.score = score
        self.title = title
        self.location = location
        self.type = domainType
        self.annotation = annotation

    def __str__(self):
        strlen = 15
        lines = ['{{title = {a}{b},'.format(a=self.title[:strlen], b='...' if len(self.title) > strlen else ''),
                 ' type = {},'.format(self.type),
                 ' location = {},'.format(self.location),
                 ' evalue = {},'.format(self.evalue),
                 ' frame = {},'.format(self.frame),
                 ' score = {}}}'.format(self.score)]
        return '\n'.join(lines)


def run_blastx(sequence):
    """Run blastx and get the list of found domains

    Arguments:
        sequence (Bio.Seq.Seq): sequence

    Returns:
        list[Domain]: list of matched domains
    """
    domains = []    
    blastx_cline = NcbiblastxCommandline(**config['blastx']['args'])
    xml_out, stderr = blastx_cline(stdin=str(sequence))

    blast_records = NCBIXML.parse(StringIO(xml_out))
    try:
        blast_record = next(blast_records).alignments
    except ValueError:
        return domains

    if blast_record:
        for alignment in blast_record:
            for hsp in alignment.hsps:
                domain = DomainDante(evalue=hsp.expect,
                                frame=hsp.frame,
                                score=hsp.score,
                                title=alignment.title)

                
                if hsp.frame[0] < 0:  # complementary strand
                    domain.location = [hsp.query_start + 3 * (hsp.align_length - hsp.gaps), hsp.query_start]
                else:
                    domain.location = [hsp.query_start, hsp.query_start + 3 * (hsp.align_length - hsp.gaps)]

                domain.type = alignment.title.split('__')[0].split('-')[1]
                domain.annotation = alignment.title.split('__')[1]
                domains.append(domain)
    return domains

def run_last(sequence):
    input_seq = open(os.getcwd() + "/tmp_seq.fa", 'w')
    input_seq.write(">seq\n")
    input_seq.write(str(sequence))
    input_seq.close()

    LAST_DB = "/opt/nester/dbs/dante_last/dante"
    query_temp = os.getcwd() + "/tmp_seq.fa"

    process = subprocess.Popen(["lastal", "-F15", LAST_DB, query_temp, "-L", "10", "-m", "70", "-p", "BL80", "-f", "TAB"],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    
    stdout, stderr = process.communicate()

    os.remove("tmp_seq.fa")
    
    return parse_last_output(stdout)

def parse_last_output(raw_output):
    domains = []
    
    lines = raw_output.decode('utf-8').split('\n')
    for line in lines:
        if len(line) == 0 or line[0] == '#':
            continue
        split_line = line.split('\t')
        domain = DomainDante(evalue = float(split_line[-1].split('=')[1]),
                             frame = [0] if split_line[9] == '+' else [-1],
                             score = int(split_line[0]),
                             title = split_line[1]
                             )
        if domain.frame[0] < 0:
            domain.location = [int(split_line[7]) + int(split_line[8]), int(split_line[7])]
        else:
            domain.location = [int(split_line[7]), int(split_line[7]) + int(split_line[8])]
                               
        domain.type = split_line[1].split('__')[0].split('-')[1]
        domain.annotation = split_line[1].split('__')[1]
        domains.append(domain)
    print(len(domains))
    return domains   
