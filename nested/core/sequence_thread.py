#!/usr/bin/env python3

import threading
from datetime import datetime

from nested.core.nester import Nester

class SequenceThread(threading.Thread):
    def __init__(self, sequence, sketcher, i, sketch, format, output_fasta_offset, output_folder, initial_threshold, threshold_multiplier):
        threading.Thread.__init__(self)
        self.sequence = sequence
        self.sketcher = sketcher
        self.i = i
        self.initial_threshold = initial_threshold
        self.threshold_multiplier = threshold_multiplier
        self.format = format
        self.output_fasta_offset = output_fasta_offset
        self.output_folder = output_folder
        self.sketch = sketch

    def run(self):
        self.sequence.id = self.sequence.id.replace('/', '--')
        seq_start_time = datetime.now()
        strlen = 15
        print('Processing {a}...'.format(a=self.sequence.id[:strlen]), end='\r')
        #try:
        nester = Nester(self.sequence, self.i, self.initial_threshold, self.threshold_multiplier)
        self.sketcher.create_gff(nester.nested_element, dirpath=self.output_folder, output_fasta_offset=self.output_fasta_offset, format=self.format)
        self.sketcher.create_solo_ltr_gff(nester.solo_ltrs, dirpath=self.output_folder)  
        if self.sketch:
            if self.format != 'default':
                self.sketcher.create_gff(nester.nested_element, dirpath=self.output_folder, output_fasta_offset=self.output_fasta_offset)            
            self.sketcher.sketch(self.sequence.id, dirpath=self.output_folder)
        seq_end_time = datetime.now()
        print('Processing {a}: DONE [{b}]'.format(a=self.sequence.id[:strlen], b=seq_end_time - seq_start_time))
        #except KeyboardInterrupt:
            # delete ltrs.fa            
        #    raise
        #except CalledProcessError:
        #    number_of_errors += 1
        #    print('Processing {}: SUBPROCESS ERROR'.format(sequence.id[:strlen]))
        #except:
        #    number_of_errors += 1
        #    print('Processing {}: UNEXPECTED ERROR:'.format(sequence.id[:strlen]), sys.exc_info()[0])
        
