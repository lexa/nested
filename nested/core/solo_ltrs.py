#!/usr/bin/env python3

import os
import subprocess
import math

from io import StringIO

from Bio import SeqIO
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastnCommandline

from nested.config.config import config
from nested.core.solo_ltr import SoloLtr
import nested.utils.intervals as intervals

class SoloLtrs(object):

    def __init__(self, seqId):
        self.sequence = ""
        self.seqId = seqId
        self.solo_ltrs = []
        self.ltrs_fasta_path = '{}/{}_ltrs.fa'.format(os.getcwd(), seqId)
        self.ltr_lengths = {}

    def find_solo_ltrs(self, sequence):
        self.sequence = sequence
        self.save_spliced_sequence()
        if os.path.exists(self.ltrs_fasta_path) and len(self.ltr_lengths.keys()) > 0:
            subprocess.run(["makeblastdb", "-in", "{}_ltrs.fa".format(self.seqId), "-dbtype", "nucl", "-out", "{}_ltrs.fa".format(self.seqId)],
                           stdout=subprocess.DEVNULL)
            blast_records = self.run_blastn()
            self._filter_intersections(self._parse_blast(blast_records))
            self.cleanup()

    def save_spliced_sequence(self):
        with open("{}_spliced.fa".format(self.seqId), 'w') as file:
            file.write(">{}_spliced\n".format(self.seqId))
            file.write(str(self.sequence))
                    
    
    def create_ltr_multifasta(self, tes, seq):
        offset = len(self.ltr_lengths.keys())
        num = 1
        with open("{}_ltrs.fa".format(self.seqId), 'a+') as file:
            for i in range(len(self.ltr_lengths.keys()), len(self.ltr_lengths.keys()) + len(tes)):
                if (intervals.length(tes[i - offset].ltr_left_location) < 100
                    or math.isnan(tes[i - offset].tsr_left[0])
                    or math.isnan(tes[i - offset].tsr_right[0])):
                    continue
                
                file.write(">ltr_" + str(num) + "\n") 
                file.write(str(seq[tes[i - offset].ltr_left_location[0]:tes[i - offset].ltr_left_location[1] + 1]) + "\n")
                self.ltr_lengths["ltr_" + str(num)] = len(str(seq[tes[i - offset].ltr_left_location[0]:tes[i - offset].ltr_left_location[1] + 1]))
                num += 1
    
    def run_blastn(self):
        args = dict(config['blastx']['args'])
        args['db'] = self.ltrs_fasta_path.replace("|", "\|")
        args['word_size'] = 10
        args['evalue'] = 1e-5
        
        blastn_cline = NcbiblastnCommandline(**args)
        xml_out, stderr = blastn_cline(stdin=str(self.sequence))
        
        return NCBIXML.parse(StringIO(xml_out))

    def _parse_blast(self, records):
        result = []
        try:
            blast_record = next(records).alignments
        except ValueError:
            return result

        if blast_record:
            for alignment in blast_record:
                for hsp in alignment.hsps:
                    if not (hsp.align_length / self.ltr_lengths[alignment.hit_def] >= 0.95):
                        continue
                    solo_ltr = SoloLtr(score = hsp.score, bits = hsp.bits)

                    if hsp.frame[0] < 0: #complementary strand
                        solo_ltr.location = [hsp.query_start - hsp.align_length, hsp.query_start]
                    else:
                        solo_ltr.location = [hsp.query_start, hsp.query_start + hsp.align_length]

                    result.append(solo_ltr)
        return result

    def _filter_intersections(self, solo_ltrs):
        result = set()
        work_list = set(solo_ltrs)
        for ltr in solo_ltrs:
            if ltr not in work_list:
                continue

            intersections = self._get_intersections(ltr, work_list)
            work_list = work_list.difference(intersections.keys())
            best_hit = self._best_hit(intersections)
            self._update_result_with_best_hit(best_hit, result)
        self.solo_ltrs = list(result)

    def _get_intersections(self, ltr, ltrs):
        result = { ltr: ltr.score * ltr.bits }
        for ltr2 in ltrs:
            if intervals.intersect(ltr.location, ltr2.location):
                result[ltr2] = ltr2.score * ltr2.bits
        return result

    def _best_hit(self, intersections):
        return max(intersections, key=lambda x: intersections[x])

    def _update_result_with_best_hit(self, hit, result):
        intersections = self._get_intersections(hit, result)
        real_best = self._best_hit(intersections)
        result.difference(intersections.keys())
        result.add(real_best)

    # refactor this
    def cleanup(self):
        if os.path.isfile("{}.nhr".format(self.ltrs_fasta_path)):
            os.remove("{}.nhr".format(self.ltrs_fasta_path))
        if os.path.isfile("{}.nin".format(self.ltrs_fasta_path)):
            os.remove("{}.nin".format(self.ltrs_fasta_path))
        if os.path.isfile("{}.nsq".format(self.ltrs_fasta_path)):
            os.remove("{}.nsq".format(self.ltrs_fasta_path))
        if os.path.isfile("{}.ndb".format(self.ltrs_fasta_path)):
            os.remove("{}.ndb".format(self.ltrs_fasta_path))
        if os.path.isfile("{}.not".format(self.ltrs_fasta_path)):
            os.remove("{}.not".format(self.ltrs_fasta_path))
        if os.path.isfile("{}.ntf".format(self.ltrs_fasta_path)):
            os.remove("{}.ntf".format(self.ltrs_fasta_path))
        if os.path.isfile("{}.nto".format(self.ltrs_fasta_path)):
            os.remove("{}.nto".format(self.ltrs_fasta_path))
        
            
