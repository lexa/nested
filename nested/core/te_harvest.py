#!/usr/bin/env python3

import os
import subprocess
import re

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

from nested.config.config import config, args_dict_to_list


class TE_harvest(object):
    """Class representing TE. Every location is in format [from, to].

    Attributes:
        ppt (list): location of ppt
        pbs (list): location of pbs
        location (list): position of TE in gene
        ltr_left_location (list): location of left ltr
        ltr_right_location (list): location of right ltr
        tsr_left (list): location of left tsr
        tsr_right (list): location of right tsr
        features (dict): dictionary of features assigned to TE (i.e. list of domains on the optimal path in graph)
        score (float): evaluated score of TE
    """

    def __init__(self, ppt=[0, 0], pbs=[0, 0], location=[0, 0],
                 ltr_left_location=[0, 0], ltr_right_location=[0, 0],
                 tsr_left=[0, 0], tsr_right=[0, 0], features={}, score=None):
        self.ppt = ppt
        self.pbs = pbs
        self.location = location
        self.ltr_left_location = ltr_left_location
        self.ltr_right_location = ltr_right_location
        self.tsr_left = tsr_left
        self.tsr_right = tsr_right
        self.features = features
        self.score = score

    def __str__(self):
        lines = ['{{location = {},'.format(self.location),
                 ' left ltr = {},'.format(self.ltr_left_location),
                 ' right ltr = {},'.format(self.ltr_right_location),
                 ' left tsr = {},'.format(self.tsr_left),
                 ' right tsr = {},'.format(self.tsr_right),
                 ' ppt = {},'.format(self.ppt),
                 ' pbs = {},'.format(self.pbs),
                 # ' features = {},'.format(self.features),
                 ' score = {}}}'.format(self.score)]
        return '\n'.join(lines)

    @staticmethod
    def run(seqid, sequence):
        """Run LTR finder on sequence and return list of transposons

        Arguments:
            seqid (str): sequence id
            sequence (Bio.Seq.Seq): sequence
            tmp_dir (str): Auxiliary existing directory

        Returns:
            list[TE]: list of found ltr pairs as a TE class
        """
        transposons = []

        if not os.path.exists('/tmp/nested'):
            os.makedirs('/tmp/nested')

        if not os.path.exists('/tmp/nested/ltr'):
            os.makedirs('/tmp/nested/ltr')

        with open('/tmp/nested/ltr/{}.fa'.format(seqid), 'w+') as tmp_file:  # prepare tmp fasta file for ltr finder
            SeqIO.write(SeqRecord(sequence, id=seqid),
                        tmp_file,
                        'fasta')

        process = subprocess.Popen(['gt', 'suffixerator', '-db', '/tmp/nested/ltr/{}.fa'.format(seqid),
            '-indexname', '/tmp/nested/ltr/{}.fa'.format(seqid), '-tis', '-suf', '-lcp', '-des', '-ssp', '-sds', '-dna'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        process.communicate()

        # call LTR harvest
        process = subprocess.Popen(
            [config['ltr_harvest']['path']] + [config['ltr_harvest']['command']] + 
                args_dict_to_list(config['ltr_harvest']['args']) + 
                ['-index', '/tmp/nested/ltr/{}.fa'.format(seqid)] + 
                ['-longoutput'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        stdout, stderr = process.communicate()
    
        parsed_output = parse_raw_output(stdout)
        for entry in parsed_output:
            params = {
                'ppt': entry['ppt'],
                'pbs': entry['pbs'],
                'location': entry['location'],
                'ltr_left_location': entry['ltr_left_location'],
                'ltr_right_location': entry['ltr_right_location'],
                'tsr_right': entry['tsr_right'],
                'tsr_left': entry['tsr_left'],
            }
            transposons.append(TE_harvest(**params))

        return transposons


def parse_raw_output(raw_output):
    """Parse raw output from LTR harvest

    Arguments:
        raw_output (str): ltr finder raw output (args -w 2)

    Returns:
        dict: parsed raw output
    """
    entries = raw_output.decode('utf-8').split(os.linesep)[13:-1]
    
    result = []
    for entry in entries:
        split_line = entry.split('  ')
        transposon = { "location": [int(split_line[0]), int(split_line[1])],
            "ltr_left_location": [int(split_line[3]), int(split_line[4])],
            "ltr_right_location": [int(split_line[8]), int(split_line[9])],
            "ppt": [float("nan"), float("nan")],
            "pbs": [float("nan"), float("nan")]
        }
        try:
            tsr_right = [int(split_line[1]) + 1, int(split_line[1]) + int(split_line[7])]
            tsr_left = [int(split_line[0]) - int(split_line[7]), int(split_line[0]) - 1]
        except:
            tsr_right = [float("nan"), float("nan")]
            tsr_left = [float("nan"), float("nan")]
        
        transposon["tsr_right"] = tsr_right
        transposon["tsr_left"] = tsr_left
        result.append(transposon)
    return result
