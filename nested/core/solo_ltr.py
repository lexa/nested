#!/usr/bin/env python3

from nested.core.te import TE

class SoloLtr(TE):

    def __init__(self, location = None, score = None, bits = None):
        TE.__init__(self)
        self.location = location
        self.score = score
        self.bits = bits        
        self.ltr_left_location = [float('nan'), float('nan')]
        self.ltr_right_location = [float('nan'), float('nan')]
        self.tsr_left = [float('nan'), float('nan')]
        self.tsr_right = [float('nan'), float('nan')]
        self.features = { "domains": [],
                          "ppt": [float('nan'), float('nan')],
                          "pbs": [float('nan'), float('nan')] }

        
    
